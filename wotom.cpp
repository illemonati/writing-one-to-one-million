#include<iostream>
#include<string>
#include<algorithm>
#include<fstream>

using namespace std;

string number_to_word(unsigned int number) {
    string units[] = {"one", "ten", "hundred", "thousand", "million"};
    string numbers[] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    string teens[] = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    string tens[] = {"ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    string result = "";

    if (number == 0) {
        return "zero";
    }

    if (number >= 1000000) {
        result += number_to_word(number/1000000);
        result += units[4] + " ";
        number = number - ((number/1000000) * 1000000);
    }
    if (number >= 1000) {
        result += number_to_word(number/1000);
        result += units[3] + " ";
        number = number - ((number/1000)*1000);
    }
    if (number >= 100) {
        result += numbers[number/100 - 1] + " " + units[2] + " ";
        number = number - ((number/100)*100);
    }
    if (number >= 10 && number < 20) {
        number = number - 10;
        result += teens[number] + " ";
    } else {
        if (number >= 20) {
            result += tens[number/10 - 1] + " ";
            number -= (number / 10 * 10);
        }
        if (number >= 1) {
            result += numbers[number - 1] + " ";
        }
    }

    return result;
}


int main(int argc, char const *argv[]) {
    ofstream out("out.txt", ios::out);
    for (unsigned int i = 0; i <= 1000000; ++i) {
        out << number_to_word(i) << endl;
    }
    out.close();
    return 0;
}
